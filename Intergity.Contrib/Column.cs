﻿using System;

namespace Integrity.Attributes
{
    /// <summary>
    /// Defines column name on database
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Column : Attribute
    {
        public string ColumnName { get; set; }

        public Column(string text)
        {
            ColumnName = text;
        }
    }
}
