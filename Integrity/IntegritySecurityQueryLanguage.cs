﻿using Integrity.Attributes;
using Integrity.Lib;
using Integrity.Lib.Database.Constants;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Integrity.Security
{
    public static class IntegritySecurityQueryLanguage
    {
        internal static AesService _aesService;
        internal static List<byte[]> _extendedKeyList;
        internal static string _entropyString;

        #region Save Methods

        public static void SaveEncryptKey(string encryptKey)
        {
            _aesService = new AesService(encryptKey, 128);
            SaveExtendedKeyList(_aesService.GetKey());
        }

        private static void SaveExtendedKeyList(byte[] key)
        {
            byte[,] extendedKey = _aesService.KeyExpansion(key);
            _extendedKeyList = TransformExtendKeyToByteList(extendedKey);
        }

        public static void SaveEntropyString(string entropyString)
        {
            byte[] entropyByte = AesService.BuildKey(entropyString, 128);
            _entropyString = Convert.ToBase64String(entropyByte);
        }

        #endregion

        #region Public Methods

        public static object ObfuscateData(object originalDataValue, string seed)
        {
            byte[] token = GenerateToken(seed, _extendedKeyList);
            return GenerateObfuscatedData(originalDataValue, token);
        }

        public static object UnobfuscateData(object obfusfcatedData, string seed)
        {
            byte[] token = GenerateToken(seed, _extendedKeyList);
            return GenerateUnobfuscateData(obfusfcatedData, token);
        }

        public static string GenerateSeedFromEntity<T>()
        {
            string stringSeed = "";

            Table tableAttribute = (Table)Attribute.GetCustomAttribute(typeof(T), typeof(Table));
            if (tableAttribute != null)
                stringSeed += tableAttribute.TableName;
            else
                throw new Exception("No table provided");

            var properties = typeof(T).GetProperties();
            IList<Column> columnAttr = AttributesUtils.GetAttributeListFromProps<Column>(properties).ToList();

            if (columnAttr.Any())
                stringSeed += String.Join($"{_entropyString}", columnAttr?.Select(x => x.ColumnName));
            else
                throw new Exception("No columns provided");

            return stringSeed;
        }

        public static string IncreaseSeedEntropy(string seed, string propertyName)
        {
            return $"{seed}{_entropyString}{propertyName}";
        }

        #endregion

        #region Private Methods
        private static byte[] GenerateSeedHash(string seed)
        {
            // Hash do semente será gerado atraves da função hash com retorno 256 bits para podermos quebra-lo em duas partes de 128
            byte[] seedHash = HashLib.Generate256SeedHash(seed);
            return seedHash; //256 bits
        }

        private static byte[] GenerateToken(string seed, List<byte[]> extendedKey)
        {
            byte[] seedHash = GenerateSeedHash(seed); //256 bits
            byte[] token = PerformXor(seedHash, extendedKey);
            return token;
        }

        private static byte[] PerformXor(byte[] seedHash, List<byte[]> extendedKey)
        {
            // Split SeedHash
            var rightSide = seedHash.Take(16).ToArray();
            var leftSide = seedHash.Skip(16).Take(16).ToArray();

            foreach (byte[] key in extendedKey)
            {
                var newRightSide = new byte[16];
                var newLeftSide = new byte[16];

                for (int i = 0; i < 16; i++)
                {
                    // Do XOR rightSide[i] with key[i] and becames newLeftSide
                    newLeftSide[i] = (byte)(rightSide[i] ^ key[i]);
                    // Do XOR with newLeftSide[i] with leftSide[i] and becames newRightSide
                    newRightSide[i] = (byte)(newLeftSide[i] ^ leftSide[i]);
                }

                //RightSide is now the xor result newRightSide
                rightSide = newRightSide;
                //LeftSide is now the xor result newLeftSide
                leftSide = newLeftSide;
            }

            return rightSide.Concat(leftSide).ToArray();
        }

        private static List<byte[]> TransformExtendKeyToByteList(byte[,] extendedKey)
        {
            List<byte[]> keyByteList = new List<byte[]>();

            int noRounds = 10;
            int idx = 4; // First position is cipher key. Idx position = 4 to take extended key
            int idx2 = 8; // First position is cipher key. Idx2 position = 8 to take extended key

            for (int k = 0; k < noRounds; k++)
            {
                byte[] temp = new byte[16];
                int current = 0;

                for (int i = idx; i < idx2; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        temp[current++] = extendedKey[i, j];
                    }
                }

                idx += 4;
                idx2 += 4;
                keyByteList.Add(temp);
            }

            return keyByteList;
        }

        private static object GenerateObfuscatedData(object originalDataValue, byte[] token)
        {
            int intToken = BitConverter.ToInt16(token);
            //string stringToken = Convert.ToBase64String(token);

            object obfusfcatedData;

            switch (Type.GetTypeCode(originalDataValue.GetType()))
            {
                //To-do Rest of TypeCode 
                case TypeCode.DateTime:
                    DateTime newDate = (DateTime)originalDataValue;
                    obfusfcatedData = newDate.AddDays(intToken % MysqlConstants.DATETIME_RANGE );
                    break;
                case TypeCode.String:
                    AesService newAes = new AesService(token);
                    obfusfcatedData = newAes.Encrypt((string)originalDataValue);
                    //obfusfcatedData = $" aes_encrypt({originalDataValue},{stringToken}) ";
                    break;
                default : 
                    obfusfcatedData = originalDataValue;
                    break;
            }

            return obfusfcatedData;
        }

        private static object GenerateUnobfuscateData(object obfusfcatedData, byte[] token)
        {
            int intToken = BitConverter.ToInt16(token) * -1;

            object originalDataValue;

            switch (Type.GetTypeCode(obfusfcatedData.GetType()))
            {
                case TypeCode.DateTime:
                    DateTime newDate = (DateTime)obfusfcatedData;
                    originalDataValue = newDate.AddDays(intToken % MysqlConstants.DATETIME_RANGE);
                    break;
                case TypeCode.String:
                    AesService newAes = new AesService(token);
                    originalDataValue = newAes.Decrypt((string)obfusfcatedData);
                    break;
                default:
                    originalDataValue = obfusfcatedData;
                    break;
            }

            return originalDataValue;
        }
        
        #endregion
    }
}
