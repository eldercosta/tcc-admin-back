﻿using Integrity.Lib;
using Integrity.Security;
using Microsoft.Extensions.Configuration;
using System;

namespace Integrity
{
    public static class IntegrityConfiguration
    {
        public static void ConfigureIntegrity(this IConfiguration configuration)
        {
            var conn = configuration.Validate("ConnectionString");
            var encryptKey = configuration.Validate("EncryptKey");
            var entropySeed = configuration.Validate("EntropySeedString");

            IntegrityDatabaseUtils.SaveConnection("INTEGRITY", conn);
            IntegritySecurityQueryLanguage.SaveEncryptKey(encryptKey);
            IntegritySecurityQueryLanguage.SaveEntropyString(entropySeed);
        }

        private static string Validate(this IConfiguration configuration, string configString) =>
            configuration[$"Integrity:{configString}"] ?? throw new ArgumentNullException(nameof(configString), $"{configString} cannot be null");
    }
}
