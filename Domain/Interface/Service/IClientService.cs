﻿using Domain.Entities.Entity;
using System.Collections.Generic;

namespace Domain.Interface.Service
{
    public interface IClientService
    {
        IList<ClientEntity> GetAll();
        ClientEntity GetById(int clientId);
        bool Save(ClientEntity client);
        bool Edit(ClientEntity client);
        bool Delete(int clientId);
    }
}
