﻿using Domain.Enum;
using System;
using System.Data;

namespace Domain.Interface.Data
{
    public interface IContext 
    {
        IDbConnection this[string key, DatabaseType dbType] { get; }
    }
}
