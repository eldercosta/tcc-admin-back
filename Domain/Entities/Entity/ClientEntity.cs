﻿using Integrity.Attributes;
using System;

namespace Domain.Entities.Entity
{
    [Table("client")]
    public class ClientEntity
    {
        [Key, Column("id")]
        public int id { get; set; }

        [Column("dataNascimento")]
        public DateTime dataNascimento { get; set; }

        [Column("nome")]
        public string nome { get; set; }

        [Column("sobrenome")]
        public string sobrenome { get; set; }

        [Column("dataCadastro")]
        public DateTime dataCadastro { get; set; }

        [Column("conta")]
        public string conta { get; set; }

        [Column("cpf")]
        public string cpf{ get; set; }

        [Column("telefone")]
        public string telefone { get; set; }
    }
}
