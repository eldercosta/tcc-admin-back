﻿using Domain.Entities.Entity;
using Domain.Interface.Data;
using Domain.Interface.Service;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Text.Json;

namespace Domain.Service
{
    public class ClientService : IClientService
    {
        private readonly ILogger<IClientService> _logger;
        private readonly IClientData _clientData;
        public ClientService(ILogger<IClientService> logger, IClientData clientData)
        {
            _logger = logger;
            _clientData = clientData;
        }

        public IList<ClientEntity> GetAll()
        {
            _logger.LogInformation($"ClientService.GetAll()");
            return _clientData.GetAllClient();
        }

        public ClientEntity GetById(int clientId)
        {
            _logger.LogInformation($"ClientService.GetById() ClientId:{clientId}");
            return _clientData.GetById(clientId);
        }

        public bool Save(ClientEntity client)
        {
            _logger.LogInformation($"ClientService.Save() Client: {JsonSerializer.Serialize(client)}");
            return _clientData.Save(client);
        }

        public bool Edit(ClientEntity client)
        {
            _logger.LogInformation($"ClientService.Update() Client: {JsonSerializer.Serialize(client)}");
            return _clientData.Edit(client);
        }

        public bool Delete(int clientId)
        {
            _logger.LogInformation($"ClientService.Delete() ClientId:{clientId}");
            return _clientData.Delete(clientId);
        }
    }
}
