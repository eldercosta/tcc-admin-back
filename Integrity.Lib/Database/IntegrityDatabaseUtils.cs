﻿
using System.Collections.Generic;
using System.Data;
using MySqlConnector;
using System.Data.SqlClient;

namespace Integrity.Lib
{
    public static class IntegrityDatabaseUtils
    {
        private static readonly Dictionary<string, string> _dbStringConnDict = new Dictionary<string, string>();

        public static void SaveConnection(string name, string value)
        {
            string upperConnName = name.ToUpper();
            if (!_dbStringConnDict.ContainsKey(upperConnName)) _dbStringConnDict.Add(upperConnName, value);
        }

        public static IDbConnection GetConnection(string connectionName, DatabaseType dbType)
        {
            string upperConnName = connectionName.ToUpper();
            IDbConnection conexao;
            switch (dbType)
            {
                case DatabaseType.MySql:
                    conexao = new MySqlConnection(); 
                    conexao.ConnectionString = _dbStringConnDict[upperConnName]; 
                    break;
                case DatabaseType.SqlServer:
                    conexao = new SqlConnection();
                    conexao.ConnectionString = _dbStringConnDict[upperConnName];
                    break;
                default:
                    return null;
            }
            return conexao;
        }
    }
}
