﻿using Domain.Entities.Entity;
using Domain.Interface.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClientController : ControllerBase
    {
        private readonly ILogger<ClientController> _logger;
        private readonly IClientService _clientService;

        public ClientController(ILogger<ClientController> logger, IClientService clientService)
        {
            _logger = logger;
            _clientService = clientService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                _logger.LogInformation("ClientController.GetAll()");
                return Ok(_clientService.GetAll());
            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{clientId}")]
        public IActionResult GetById(int clientId)
        {
            try
            {
                _logger.LogInformation($"ClientController.GetById() ClientId {clientId}");
                return Ok(_clientService.GetById(clientId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        public IActionResult Save(ClientEntity client)
        {
            try
            {
                _logger.LogInformation($"ClientController.Save() Client: {JsonSerializer.Serialize(client)}");
                _clientService.Save(client);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPut]
        public IActionResult Update(ClientEntity client)
        {
            try
            {
                _logger.LogInformation($"ClientController.Update() Client: {JsonSerializer.Serialize(client)}");
                _clientService.Edit(client);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete("{clientId}")]
        public IActionResult Delete(int clientId)
        {
            try
            {
                _logger.LogInformation("ClientController.Delete()");
                _clientService.Delete(clientId);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
