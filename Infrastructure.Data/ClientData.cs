﻿using Domain.Entities.Entity;
using Domain.Interface.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Infrastructure.Data
{
    public class ClientData //: IClientData
    {
        private readonly ILogger<IClientData> _logger;
        public ClientData(ILogger<IClientData> logger) 
        {
            _logger = logger;
        }

        public IList<ClientEntity> GetAllClient()
        {
            using var connection = ConnectionUtils.CreateMySqlDbConnection("INTEGRITY");
            try
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "SELECT id,  nome,  dataNascimento,  dataCadastro ,  conta,  cpf,  telefone FROM client;";
                command.CommandType = CommandType.Text;
                var reader = command.ExecuteReader();

                var dataTable = new DataTable();
                dataTable.Load(reader);

                return (from dataRow in dataTable.Select()
                        select new ClientEntity()
                        {
                            id = dataRow.Field<int>("id"),
                            nome = dataRow["nome"]?.ToString(),
                            sobrenome = dataRow["sobrenome"]?.ToString(),
                            dataNascimento = dataRow.Field<DateTime>("dataNascimento"),
                            dataCadastro = dataRow.Field<DateTime>("dataCadastro"),
                            conta = dataRow.Field<string>("conta"),
                            cpf = dataRow.Field<string>("cpf"),
                            telefone = dataRow.Field<string>("telefone"),
                        }).ToList();
            } catch
            {
                _logger.LogError($"Error retrieving Client List");
                throw;
            }
            finally
            {
                connection.Close();
            }
        }

        public ClientEntity GetById(int clientId)
        {
            using var connection = ConnectionUtils.CreateMySqlDbConnection("INTEGRITY");
            try
            {
                connection.Open();
                var command = connection.CreateCommand();
                var parameter = command.CreateParameter();

                parameter.Direction = ParameterDirection.Input;
                parameter.ParameterName = "Id";
                parameter.Value = clientId;
                command.Parameters.Add(parameter);

                command.CommandText = "SELECT id,  nome,  dataNascimento,  dataCadastro ,  conta,  cpf,  telefone FROM client where id = @Id;";
                command.CommandType = CommandType.Text;
                var reader = command.ExecuteReader();

                var dataTable = new DataTable();
                dataTable.Load(reader);

                return (from dataRow in dataTable.Select()
                        select new ClientEntity()
                        {
                            id = dataRow.Field<int>("id"),
                            nome = dataRow["nome"]?.ToString(),
                            sobrenome = dataRow["sobrenome"]?.ToString(),
                            dataNascimento = dataRow.Field<DateTime>("dataNascimento"),
                            dataCadastro = dataRow.Field<DateTime>("dataCadastro"),
                            conta = dataRow.Field<string>("conta"),
                            cpf = dataRow.Field<string>("cpf"),
                            telefone = dataRow.Field<string>("telefone"),
                        }).FirstOrDefault();
            }
            catch
            {
                _logger.LogError($"Error retrieving Client List");
                throw;
            }
            finally
            {
                connection.Close();
            }
        }

        public bool Save(ClientEntity client)
        {
            using var connection = ConnectionUtils.CreateMySqlDbConnection("INTEGRITY");
            try
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = @"insert into client ( id,  nome,  dataNascimento,  dataCadastro ,  conta,  cpf,  telefone) 
                                        values (@Id,  @Nome,  @DataNascimento,  @DataCadastro ,  @Conta,  @Cpf,  @Telefone)";
                //command.CreateDatabaseParameters(client);
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error retrieving Client List {ex.Message}");
                throw;
            }
            finally
            {
                connection.Close();
            }
        }

        public bool Edit(ClientEntity client)
        {
            using var connection = ConnectionUtils.CreateMySqlDbConnection("INTEGRITY");
            try
            {
                connection.Open();
                var command = connection.CreateCommand();

                command.CommandText = @"update client 
                                        set nome = @Nome, 
                                        dataNascimento = @DataNascimento,  
                                        dataCadastro = @DataCadastro,  
                                        conta = @Conta,  
                                        cpf = @Cpf,  
                                        telefone = @Telefone
                                        where id = @Id";
                //command.CreateDatabaseParameters(client);
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error retrieving Client List {ex.Message}");
                throw;
            }
            finally
            {
                connection.Close();
            }
        }

        public bool Delete(int clientId)
        {
            using var connection = ConnectionUtils.CreateMySqlDbConnection("INTEGRITY");
            try
            {
                connection.Open();
                var command = connection.CreateCommand();
                var parameter = command.CreateParameter();

                parameter.Direction = ParameterDirection.Input;
                parameter.ParameterName = "Id";
                parameter.Value = clientId;
                command.Parameters.Add(parameter);

                command.CommandText = @"delete from client where id = @Id;";
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error retrieving Client List {ex.Message}");
                throw;
            }
            finally
            {
                connection.Close();
            }
        }

    }
}
