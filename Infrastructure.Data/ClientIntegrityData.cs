﻿using Domain.Entities.Entity;
using Domain.Interface.Data;
using System.Collections.Generic;
using System.Linq;
using Integrity.Lib;
using Integrity.Repository;

namespace Infrastructure.Data
{
    public class ClientIntegrityData : IntegrityBaseRepository<ClientEntity>, IClientData
    {
        public ClientIntegrityData() : base("Integrity", DatabaseType.MySql)
        {
        }

        public IList<ClientEntity> GetAllClient() => GetAll().ToList(); 

        public ClientEntity GetById(int clientId) => Get(new { id = clientId });

        public bool Save(ClientEntity client) => Add(client);

        public bool Edit(ClientEntity client) => Update(client);

        public bool Delete(int clientId) => Delete(new { id = clientId });

    }
}
