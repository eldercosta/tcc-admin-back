﻿using MySqlConnector;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Infrastructure.Data
{
    public static class ConnectionUtils
    {
        private static readonly Dictionary<string, string> _databaseConn = new Dictionary<string, string>();

        public static void SaveConnection(string name, string value)
        {
            if (!_databaseConn.ContainsKey(name)) _databaseConn.Add(name, value);
        }

        public static IDbConnection CreateMySqlDbConnection(string connectionName)
        {
            IDbConnection dbConnection = new MySqlConnection();
            dbConnection.ConnectionString = _databaseConn[connectionName];
            return dbConnection; 
        }

        public static IDbConnection CreateSqlServerDbConnection(string connectionName)
        {
            IDbConnection dbConnection = new SqlConnection();
            dbConnection.ConnectionString = _databaseConn[connectionName];
            return dbConnection;
        }
    }
}
