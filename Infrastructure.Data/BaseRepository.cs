﻿using Integrity.Mapper;
using Domain.Enum;
using Domain.Interface.Data;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Infrastructure.Data
{
    public class BaseRepository<T> where T : class
    {
        protected string _command = string.Empty;
        private readonly IContext _context;

        internal IDbConnection _connection => _context[_schemaName, _dbType];
        internal readonly string _schemaName;
        internal readonly DatabaseType _dbType;

        protected BaseRepository(IContext context, string schemaName, DatabaseType dbType)
        {
            _schemaName = schemaName;
            _dbType = dbType;
            _context = context;
        }

        #region Protected Methods
        protected T Get(int id)
        {
            try
            {
                return _connection.GetAll<T>().ToList().First();
            }
            catch
            {
                throw;
            }
        }

        protected IEnumerable<T> GetAll()
        {
            try
            {
                return _connection.GetAll<T>();
            }
            catch
            {
                throw;
            }
        }

        protected bool Add(T entity)
        {
            try
            {
                return _connection.Insert(entity);
            }
            catch
            {
                throw;
            }
        }

        protected bool Delete(T entity)
        {
            try
            {
                return _connection.Delete(entity);
            }
            catch
            {
                throw;
            }
        }

        protected bool Update(T entity)
        {
            throw new System.NotImplementedException();
        }

        protected object Query(string command, string parameters)
        {
            throw new System.NotImplementedException();
        }

        protected K QueryFirstOrDefault<K>(object parameter = null)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}
